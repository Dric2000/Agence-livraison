<div style="margin-top:5px;">
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="float-left mt-4 pull-left">
              <h2>LISTE DES CLIENTS</h2>
          </div>
          <div class="float-right mt-4 pull-right">
              <input wire:model="search" class="form-control" type="text" placeholder="Search Users...">
          </div>
        </div>
    </div>

    <div class="row">
        @if ($clients->count())
        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th wire:click="sortBy('first_name')">
                        Nom
                        @include('layouts.setOrderField', ['field' => 'first_name'])
                    </th>
                    <th wire:click="sortBy('last_name')">
                        Prénoms
                        @include('layouts.setOrderField', ['field' => 'last_name'])
                    </th>
                    <th>
                        Genre
                        @include('layouts.setOrderField', ['field' => 'gender'])
                    </th>
                    <th wire:click="sortBy('age')">
                        Age
                        @include('layouts.setOrderField', ['field' => 'age'])
                    </th>
                    <th wire:click="sortBy('country')">
                        Pays de résidence
                        @include('layouts.setOrderField', ['field' => 'country'])
                    </th>
                    <th wire:click="sortBy('city')">
                        Ville de résidence
                        @include('layouts.setOrderField', ['field' => 'city'])
                    </th>
                    <th>Adresse</th>
                    <th>Email</th>
                    <th wire:click="sortBy('username')">
                        Nom d'utilisateur
                        @include('layouts.setOrderField', ['field' => 'username'])
                    </th>
                    <th>Mots de passe</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $client)
                    <tr>
                        <td> 
                            <figure class="image is-10*10"> 
                                <img src="https://eu.ui-avatars.com/api/?name={{$client->first_name}}&background=random" class="rounded-circle" style = "width:20px;"> 
                            </figure>
                        </td>
                        <td>{{ $client->first_name }}</td>
                        <td>{{ $client->last_name }}</td>
                        <td>{{ $client->gender }}</td>
                        <td>{{ $client->age }}</td>
                        <td>{{ $client->country }}</td>
                        <td>{{ $client->city }}</td>
                        <td>{{ $client->house }}</td>
                        <td>{{ $client->email }}</td>
                        <td>{{ $client->username }}</td>
                        <td>{{ $client->password }}</td>
                        
                       
                    </tr>
                @endforeach
            </tbody>
        </table>

        

        @else
            <div class="alert alert-warning">
                Aucun résultat ne coorespond à ce mot-clé !!!
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col">
            {{ $clients->links() }}
        </div>
    </div>

</div>