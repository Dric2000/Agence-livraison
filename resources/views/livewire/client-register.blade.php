<div>




    <form wire:submit.prevent>
        
        {{--step 1 --}}
        @if($currentStep == 1)
        <div class="step-one">
            <div class="card">
                <div class="card-header bg-secondary text-white">Etape 1/3 - Vos informations personnelles</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Votre nom</label>
                                <input type="text" class="form-control" placeholder="Entrez votre nom" wire:model="first_name">
                                <span class="text-danger">@error('first_name'){{ $message }}@enderror</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Votre prénom</label>
                                <input type="text" class="form-control" placeholder="Entrez votre prénom" wire:model="last_name">
                                 <span class="text-danger">@error('last_name'){{ $message }}@enderror</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="gender">Votre genre</label>
                                <select class="form-control" wire:model="gender">
                                    <option value="" selected>Choisissez votre genre</option>
                                    <option value="male">Homme</option>
                                    <option value="female">Femme</option>
                                </select>
                                 <span class="text-danger">@error('gender'){{ $message }}@enderror</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Votre âge</label>
                                <input type="text" class="form-control" placeholder="Entrez votre âge" wire:model="age">
                                 <span class="text-danger">@error('age'){{ $message }}@enderror</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif


        {{-- step-2 --}}
        @if($currentStep == 2)
        <div class="step-two">
            <div class="card">
                <div class="card-header bg-secondary text-white">Etape 2/3 - Vos informations de localisation</div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Votre pays</label>
                        <input type="text" class="form-control" placeholder="Entrez votre pays de résidence" wire:model="country">
                         <span class="text-danger">@error('country'){{ $message }}@enderror</span>
                    </div>
                    <div class="col-md-6">
                        <label for="">Votre ville</label>
                        <input type="text" class="form-control" placeholder="Entrez votre ville de résidence" wire:model="city">
                         <span class="text-danger">@error('city'){{ $message }}@enderror</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Votre domicile</label>
                        <input type="text" class="form-control" placeholder="Entrez votre domicile" wire:model="house">
                         <span class="text-danger">@error('house'){{ $message }}@enderror</span>
                    </div>
                </div>
            </div>
        </div>
        @endif


        {{-- step-3 --}}
        @if($currentStep == 3)
        <div class="step-three">
            <div class="card">
                <div class="card-header bg-secondary text-white">Etape 3/3 - Vos informations de connexion</div>
                <div class="card-body">
                    <div class="row">
                        <label for="">Votre email</label>
                        <input type="text" class="form-control" placeholder="Entrez votre adresse email" wire:model="email">      
                         <span class="text-danger">@error('email'){{ $message }}@enderror</span>
                    </div>
                    <div class="row">
                        <label for="">Votre nom d'utilisateur</label>
                        <input type="text" class="form-control" placeholder="Entrez votre nom d'utilisateur" wire:model="username">
                         <span class="text-danger">@error('username'){{ $message }}@enderror</span>
                    </div>
                    <div class="row">
                        <label for="">Votre mot de passe</label>
                        <input type="text" class="form-control" placeholder="Créez un mot de passe" wire:model="password">
                         <span class="text-danger">@error('password'){{ $message }}@enderror</span>
                    </div>
                </div>
            </div>
        </div>
        @endif


        <div class="actions-buttons d-flex justify-content-between bg-red pt-2 pb2">

            @if($currentStep == 1)
                <div></div>
            @endif

            @if($currentStep == 2 || $currentStep == 3) 
                <button type="button" class="btn btn-md btn-secondary" wire:click="decreaseStep()">Retour</button>
            @endif

            @if($currentStep == 1 || $currentStep ==2)
                <button type="button" class="btn btn-md btn-success" wire:click="increaseStep()">Suivant</button>
            @endif

            @if($currentStep == 3)
                <button type="submit" class="btn btn-md btn-primary" wire:click='register()'>Enregistrer</button>
            @endif


            
            
            
        </div>

    </form>
    




</div>
