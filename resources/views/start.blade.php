<!DOCTYPE html>
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Delivery</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="{{ asset('home/favicon.ico') }}">

        <!--Google Font link-->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">

        <link rel="stylesheet" href="{{ asset('home/assets/css/swiper.min.css') }}">
        <link rel="stylesheet" href="{{ asset('home/assets/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('home/assets/css/iconfont.css') }}">
        <link rel="stylesheet" href="{{ asset('home/assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('home/assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('home/assets/css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('home/assets/css/bootsnav.css') }}">



        <!--For Plugins external css-->
        <!--<link rel="stylesheet" href="assets/css/plugins.css" />-->
        <!--Theme custom css -->
        <link rel="stylesheet" href="{{ asset('home/assets/css/style.css') }}">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="{{ asset('home/assets/css/responsive.css') }}" />

        <script src="{{ asset('home/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
    </head>

    <body data-spy="scroll" data-target=".navbar-collapse">


        <!-- Preloader -->
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_four"></div>
                </div>
            </div>
        </div><!--End off Preloader -->


        <div class="culmn">
            <!--Home page style-->


            <nav class="navbar navbar-default bootsnav navbar-fixed white no-background">
                <div class="container">  

                    <!-- Start Atribute Navigation -->
                    <div class="attr-nav">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>        
                    <!-- End Atribute Navigation -->


                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="#brand">
                            <img src="{{ asset('home/assets/images/logo.png') }}" class="logo logo-display" alt="">
                            <img src="{{ asset('home/assets/images/footer-logo.png') }}" class="logo logo-scrolled" alt="">
                        </a>

                    </div>
                    <!-- End Header Navigation -->

                    <!-- navbar menu -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-center">
                            <li><a href="#home">Accueil</a></li>                    
                            <li><a href="#features">Nos services</a></li>
                            <li><a href="#reviews">A propos</a></li>
                            <li><a href="#download">Contact</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>   
            </nav>

            <!--Home Sections-->

            <section id="home" class="home">
                <div class="container">
                    <div class="row">
                        <div class="main_home">
                            <div class="col-md-6">
                                <div class="home_text">
                                    <h1 class="text-white">Votre solution de livraison à l'internationale</h1>
                                </div>

                                <div class="home_btns m-top-40">
                                    <a href="{{ route('subscription')}}" class="btn btn-danger m-top-20">S'inscrire</a>
                                    <a href="" class="btn btn-primary m-top-20">Se connecter</a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="phone_one phone_attr1 text-center sm-m-top-50">
                                    <div class="attr_deg">9&deg;</div>
                                    <div class="attr_rio text-uppercase">Rio</div>
                                    <div class="attr_sun text-uppercase text-white">Sunny</div>
                                    <div class="attr_lon text-uppercase text-white">London</div>
                                    <img src="{{ asset('home/assets/images/livraison.png') }}" alt="" />
                                </div>
                            </div>

                        </div>
                        <div class="scrooldown">
                            <a href="#features"><i class="fa fa-chevron-down"></i></a>
                        </div>

                    </div><!--End off row-->
                </div><!--End off container -->
            </section> <!--End off Home Sections-->



            <!--Featured Section-->
            <section id="features" class="features">
                <div class="container">
                    <div class="row">
                        <div class="main_features p-top-100">
                            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                <div class="head_title text-center">
                                    <h2>Nos services</h2>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <i class="icon icon-diamond6"></i>
                                    </div>
                                    <div class="f_item_text">
                                        <h3>LIvraison de colis</h3>
                                        <p>De n'importe quel point de départ à n'importe quel destination,  
                                            nous livrons vos colis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <i class="icon icon-layers2"></i>
                                    </div>
                                    <div class="f_item_text">
                                        <h3>Transport de personnes</h3>
                                        <p>Pour vos voyages ou déplacements, 
                                            nous assurons votre transport en toute sécurité</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <i class="icon icon-business-32"></i>
                                    </div>
                                    <div class="f_item_text">
                                        <h3>Tracking</h3>
                                        <p>Nous réalisons un suivi total de vos commandes depuis le point de départ à la
                                            destination finale
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off Featured Section-->



            <!--screen short section-->

            <section id="screen_area" class="screen_area" style="margin-top: 200px;">
                <div class="container">
                    <div class="row">
                        <div class="main_screen">
                            <div class="col-md-8 col-md-offset-2 col-sm-12">
                                <div class="head_title text-center">
                                    <h2>A propos</h2>
                                    <h5>Delivery, la solution</h5>
                                </div>
                            </div>

                            <!-- Screen01-->
                            <div class="screen01 roomy-50">
                                <div class="col-md-6">
                                    <div class="screen01_img text-center">
                                        <div class="attr_cloudy text-white">Cloudy</div>
                                        <div class="attr_deg2 text-white">15 &deg;</div>
                                        <img src="{{ asset('home/assets/images/rapide.png') }}" alt="" />
                                        <h1 class="cloudy">Cloudy</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="screen01_content m-top-100">
                                        <div class="screen01_content_img ">
                                            <img src="{{ asset('home/assets/images/cloud1.png') }}" alt="" />
                                        </div>
                                        <h2 class="m-top-40">Livraison rapide</h2>
                                        <p class="m-top-20">Delivery propose des temps de livraison imbattables sur le marché.
                                            Expédiez ou recevez vos colis en des temps reccords
                                        </p>

                                        <ul class="list-inline m-top-50">
                                            <li class="text-center">95%</li>
                                            <li class="text-center">9 &deg; </li>
                                            <li class="text-center">Rainy</li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!--End off Screen01-->

                            <!-- Screen02-->
                            <div class="screen02 roomy-50 m-top-100">
                                <div class="col-md-6 col-md-push-6">
                                    <div class="screen02_img text-center">
                                        <div class="attr_sun2 text-white">Cloudy</div>
                                        <div class="attr_deg3 text-white">25 &deg;</div>
                                        <img src="{{ asset('home/assets/images/screen02.png') }}" alt="" />
                                        <h1 class="sunny">Sunny</h1>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-pull-6">
                                    <div class="screen02_content m-top-100">
                                        <div class="screen02_content_img ">
                                            <img src="{{ asset('home/assets/images/sun1.png') }}" alt="" />
                                        </div>
                                        <h2 class="m-top-40">Informatisez votre logistique</h2>
                                        <p class="m-top-20">Gérez toute votre logistique avec Delivery et gardez un oeil sur 
                                            toutes vos commandes </p>

                                        <ul class="list-inline m-top-50">
                                            <li class="text-center">95%</li>
                                            <li class="text-center">9 &deg; </li>
                                            <li class="text-center">Rainy</li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!--End off Screen02-->



                        </div>
                    </div><!--End off row-->
                </div><!--End off container-->
            </section><!--End off Screen01 Area Section -->



            <!--People Section-->
            <section id="reviews" class="reviews m-top-100">
                <div class="container">
                    <div class="row">
                        <div class="main_reviews">
                            <div class="col-md-8 col-md-offset-2 col-sm-12">
                                <div class="head_title text-center">
                                    <h2>Pourquoi nous choisir ?</h2>
                                </div>
                            </div>

                            <div class="reviews_content">
                                <div class="col-md-6">
                                    <div class="reviews_item m-top-40">
                                        <div class="reviews_item_icon pull-left">“</div>
                                        <h3>Delivery est l'une des seles applications offrant des services de livraison à l'internationale. </h3>
                                        <div class="reviews_item_icon1 pull-right text-right">“</div>
                                    </div>
                                </div><!-- End off col-md-6 -->

                                <div class="col-md-6">
                                    <div class="reviews_item m-top-40">
                                        <div class="reviews_item_icon pull-left">“</div>
                                        <h3>Nos tarifs sont les meilleurs,
                                            quelque soit les distances et les délais.</h3>
                                        <div class="reviews_item_icon1 pull-right text-right">“</div>
                                    </div>
                                </div><!-- End off col-md-6 -->
                                <div class="col-md-6">
                                </div><!-- End off col-md-6 -->
                                <div class="col-md-6">
                                </div><!-- End off col-md-6 -->

                            </div>
                        </div><!-- End off Main People -->
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off People Section-->


            <!--App Download Section-->
            <section id="download" class="download m-top-100">
                <div class="download_overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="main_download ">
                            <div class="col-md-6">
                                <div class="download_item roomy-100">
                                    <h2 class="text-white">Rejoignez-nous maintenant</h2>
                                    <h5 class="text-white m-top-20">Remplissez le formulaire ci-contre pour commencer à lancer des commandes.</h5>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="download_item m-top-70">
                                    <img class="app_right" src="assets/images/appdownload1.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- scroll up-->
            <div class="scrollup">
                <a href="#"><i class="fa fa-chevron-up"></i></a>
            </div><!-- End off scroll up -->
            
            
            <footer id="footer" class="footer bg-black">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-default bootsnav footer-menu no-background">
                                <div class="container">  

                                    <!-- Start Atribute Navigation -->
                                    <div class="attr-nav">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        </ul>
                                    </div>        
                                    <!-- End Atribute Navigation -->


                                    <!-- Start Header Navigation -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-footer">
                                            <i class="fa fa-bars"></i>
                                        </button>
                                        <a class="navbar-brand" href="#brand"><img src="assets/images/footer-logo.png" class="logo" alt=""></a>
                                    </div>
                                    <!-- End Header Navigation -->

                                    <!-- navbar menu -->
                                    <div class="collapse navbar-collapse" id="navbar-footer">
                                        <ul class="nav navbar-nav navbar-center">
                                            <li><a href="#home">Accueil</a></li>                    
                                            <li><a href="#features">Nos services</a></li>
                                            <li><a href="#reviews">A propos</a></li>
                                            <li><a href="#download">Contact</a></li>
                                        </ul>
                                    </div><!-- /.navbar-collapse -->
                                </div>   
                            </nav>
                        </div>
                        <div class="divider"></div>
                        <div class="col-md-12">
                            <div class="main_footer text-center p-top-40 p-bottom-30">
                                <p class="wow fadeInRight" data-wow-duration="1s">
                                    Made with 
                                    <i class="fa fa-heart"></i>
                                    by 
                                    <a target="_blank" href="http://bootstrapthemes.co">Bootstrap Themes</a> 
                                    2016. All Rights Reserved
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>




        </div>

        <!-- JS includes -->

        <script src="{{ asset('home/assets/js/vendor/jquery-1.11.2.min.js') }}"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>

        <script src="{{ asset('home/assets/js/jquery.magnific-popup.js') }}"></script>
        <script src="{{ asset('home/assets/js/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('home/assets/js/swiper.min.js') }}"></script>
        <script src="{{ asset('home/assets/js/jquery.collapse.js') }}"></script>
        <script src="{{ asset('home/assets/js/bootsnav.js') }}"></script>



        <script src="{{ asset('home/assets/js/plugins.js') }}"></script>
        <script src="{{ asset('home/assets/js/main.js') }}"></script>

    </body>
</html>