<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Inscription</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('home/assets/css/bootstrap.min.css') }}">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>

    <body class="d-flex justify-content-center p-4 pt-4 mt-50">
        <div class="card">
            <div class="card-header bg-success text-white">Inscription réussie</div>
            <div class="card-body">
                Félicitations <b> {{ request()->name }}</b><br>
                Votre inscription est approuvée<br>
                Vous pouvez à présent vous connecter !
                <br><br>
                <a href="{{ route('start') }}" class=" btn btn-primary ">Retour à l'accueil</a>
            </div>
        </div>
    </body>
</html>