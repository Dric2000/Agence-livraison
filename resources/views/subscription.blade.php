<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Inscription</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('home/assets/css/bootstrap.min.css') }}">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        @livewireStyles
    </head>

    <body>



        <div class="container">
            <div class="row" style="margin-top:50px;">
                <div class="col-md-6 offset-md-3">
                    <h1>Inscrivez-vous maintenant</h1>
                    @livewire('client-register')
                </div>
            </div>
        </div>
        




        @livewireScripts
    </body>
</html>