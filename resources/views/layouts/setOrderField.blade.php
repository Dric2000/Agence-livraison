@if ($sortField !== $field)
    <i class="bi bi-filter-square"></i>
@elseif ($sortAsc)
    <i class="bi bi-arrow-up-square-fill"></i>
@else
    <i class="bi bi-arrow-down-square-fill"></i>
@endif