<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Client;

use Livewire\WithPagination;

class ClientsTable extends Component
{
    use WithPagination;

    public string $search = '';
    
    public string $sortField = 'first_name';

    public $sortAsc = true;

    
    public function sortBy($field){
        if($this->sortField === $field){
            $this->sortAsc = !$this->sortAsc;
        }else{
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function render()
    {
        return view('livewire.clients-table', [
            'clients'=> Client::where('first_name', 'LIKE', "%{$this->search}%")
                ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                ->Paginate(10)
        ]);
    }
}
