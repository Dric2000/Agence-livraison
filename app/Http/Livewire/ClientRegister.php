<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Client;

class ClientRegister extends Component
{
    use WithFileUploads;
    public $first_name;
    public $last_name;
    public $gender;
    public $age;
    public $country;
    public $city;
    public $house;
    public $email;
    public $username;
    public $password;

    public $totalSteps = 3;
    public $currentStep = 1;

    public function mount(){
        $this->currentStep = 1;
    }

    public function render()
    {
        return view('livewire.client-register');
    }

    public function increaseStep(){
        $this->resetErrorBag();
        $this->validateData();
        $this->currentStep++;
        if($this->currentStep >$this->totalSteps){
            $this->currentStep = $this->totalSteps;
        }
    }

    public function decreaseStep(){
        $this->resetErrorBag();
        $this->currentStep--;
        if($this->currentStep <1){
            $this->currentStep = 1;
        }
    }

    public function validateData(){
        if($this->currentStep == 1){                                                                                      
            $this->validate([
                'first_name'=>'required|string',
                'last_name'=>'required|string',
                'gender'=>'required',
                'age'=>'required|digits:2'
            ]);
        }
        elseif($this->currentStep == 2){
            $this->validate([
                'country'=>'required|string',
                'city'=>'required|string',
                'house'=>'required'                                                                ]); 
        }
    }

    public function register(){
        $this->resetErrorBag();
        if($this->currentStep == 3){
            $this->validate([
                'email'=>'required',
                'username'=>'required',
                'password'=>'required'
            ]);
        }

        $values = array(
            "first_name"=>$this->first_name,
            "last_name"=>$this->last_name,
            "gender"=>$this->gender,
            "age"=>$this->age,
            "country"=>$this->country,
            "city"=>$this->city,
            "house"=>$this->house,
            "email"=>$this->email,
            "username"=>$this->username,
            "password"=>$this->password
        );

        Client::insert($values);
        $this->reset();
        $this->currentStep = 1;
    }
}
