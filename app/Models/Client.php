<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $primarykey = "id";
    public $timestamps = false;

    protected $fillable = [
        'first_name',
        'last_name',
        'age',
        'gender',
        'country',
        'city',
        'house',
        'email',
        'username',
        'password'
    ];

 
}
